package com.task05_log4j.Task1;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4j2HelloWorldExample {

  private static final Logger LOGGER1 = LogManager.getLogger("Test");
  private static final Logger LOGGER2 = LogManager.getLogger("MailTest");

  public static void main(String[] args) {

    for (int i = 0; i <= 50; i++) {
      if (i <= 10) {
        LOGGER2.debug("Debug i= " + i);
      }
      if (i >= 10 && i <= 50) {

        LOGGER2.info("Info i=" + i);
        if (i <= 35) {
          LOGGER2.warn("Warn i = " + i);
        }
        if (i >= 35 || i <= 40) {
          LOGGER2.error("Erroring i=" + i);
        }
        if (i >= 40) {
          LOGGER2.fatal("Fatal i = " + i);
        }

      }
    }
    LOGGER2.error("New Error Log");
  }
}